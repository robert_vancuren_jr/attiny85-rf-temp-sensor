# README #

### ATTiny85 wireless temperature sensor ###
Using an ATTiny85 to build a low power wireless temperature sensor

### How do I get set up? ###
Get your hands on an [ATTiny85](http://www.atmel.com/devices/ATTINY85.aspx)
and a [DS1621](http://www.maximintegrated.com/en/products/analog/sensors-and-sensor-interface/DS1621.html/tb_tab0)

I used the [Tiny Core for Arduino](http://code.google.com/p/arduino-tiny/)

In addition to that you will need the following libraries:

* [Manchester](https://github.com/mchr3k/arduino-libs-manchester)
* [Tiny Wire M](https://github.com/adafruit/TinyWireM)

I used a [Pocket AVR Programmer](https://www.sparkfun.com/products/9825) to program my ATTiny85 but feel free to use what ever you like.