#include <avr/sleep.h>
#include <Manchester.h>
#include <TinyWireM.h>

#define SENSOR_ID 1
#define TX_PIN 3
#define TEMP_SENSOR B1001000

#define READ_TEMPERATURE 0xAA
#define ACCESS_CONFIG 0xAC
#define START_CONVERT 0xEE

byte oneShotConfig = B00000001;
byte doneMask = B10000000;

uint8_t data[2] = {0, 0};

void setup() {
    TinyWireM.begin();
    writeConfig(TEMP_SENSOR, oneShotConfig);
    
    //NOTE if you are running at 1MHz you need to include this line
    man.workAround1MhzTinyCore();

    man.setupTransmit(TX_PIN, MAN_1200);
    enableWatchDogTimer();
}

void enableWatchDogTimer() {
    //Code from http://brohogan.blogspot.com/search/label/ATtiny85

    cli(); // Disable global interrupts 

    MCUSR = 0x00; // clear all reset flags
    WDTCR |= (1 << WDCE) | (1 << WDE); // set WD_ChangeEnable & WD_resetEnable to to protect WDTCR
    WDTCR = (1 << WDP3) | (1 << WDP0); // Prescale watchdog timer to 8 seconds which is max.
    WDTCR |= (1 << WDIE); // Enable watchdog timer interrupts

    sei(); // Enable global interrupts
}


void loop() {
    oneShotConversion(TEMP_SENSOR);

    char temp = getOneByteTemp(TEMP_SENSOR);
    temp = celsiusToFahrenheit(temp);

    transmitTemp(temp);

    powerDown();
}

void transmitTemp(char temp) {
    data[0] = SENSOR_ID;
    data[1] = temp;
    
    man.transmitArray(2, data);
}

void powerDown() {
    //turn off ADC
    ADCSRA &= (0 << ADEN);

    set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Use the Power Down sleep mode
    sleep_mode(); // System sleep BEGINS and ENDS here!
    sleep_disable();

    //enable ADC
    ADCSRA |= (1 << ADEN);
}

char celsiusToFahrenheit(char celsius) {
    return celsius * (9.0f / 5.0f) + 32.0f;
}

void readTemperature(int deviceId) {
    TinyWireM.beginTransmission(deviceId);
    TinyWireM.write(READ_TEMPERATURE);
    TinyWireM.endTransmission();
}

char getOneByteTemp(int deviceId) {
    readTemperature(deviceId);

    TinyWireM.requestFrom(deviceId, 1);
    
    //The value returned from the DS1621
    //is a signed byte (Two's complement)
    //casting as a char because a char is a signed byte
    return (char)TinyWireM.read();
}

void oneShotConversion(int deviceId) {
    requestConversion(deviceId);
    waitForConversionToFinish(deviceId);
}

void requestConversion(int deviceId) {
    TinyWireM.beginTransmission(deviceId);
    TinyWireM.write(START_CONVERT);
    TinyWireM.endTransmission();
}

void waitForConversionToFinish(int deviceId) {
    //This method just blocks until the conversion
    //is complete
    while (!isConversionDone(deviceId)) {
    } 
}

bool isConversionDone(int deviceId) {
    TinyWireM.beginTransmission(deviceId);
    TinyWireM.write(ACCESS_CONFIG);
    TinyWireM.endTransmission();
    TinyWireM.requestFrom(deviceId, 1);

    byte config = TinyWireM.read();
    return config & doneMask;
}

void writeConfig(int deviceId, byte config) {
    TinyWireM.beginTransmission(deviceId);
    TinyWireM.write(ACCESS_CONFIG);
    TinyWireM.write(config);
    TinyWireM.endTransmission();
}
