//Running this on an Arduino Uno
#include <Manchester.h>

#include <SPI.h>
#include <Wire.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

#define RX_PIN 4

char clearLine[] = "****            ";

byte pos = 0;
#define BUFFER_SIZE 2
uint8_t buffer[BUFFER_SIZE];

unsigned long currentTime = 0;
unsigned long lastReading = 0;

void setup()   {
    lcd.begin(16, 2);

    lcd.setCursor(0, 0);
    lcd.print("RF temp readings");

    lcd.setCursor(0, 1);
    lcd.print("Waiting...");


    man.setupReceive(RX_PIN, MAN_1200);
    man.beginReceiveArray(BUFFER_SIZE, buffer);
}


void loop() {
    if (man.receiveComplete()) {

        lastReading = millis();

        byte address = buffer[0];
        char temp = buffer[1];

        switch (address) {
            case 1:
                printOfficeTemp(temp);
                break;
            case 2:
                printRoamingTemp(temp);
                break;
        }

        man.beginReceiveArray(BUFFER_SIZE, buffer);
    }

    currentTime = millis();

    if ((currentTime - lastReading) > 30000) {
        //TODO track each sensor
        lcd.setCursor(0, 1);
        lcd.print(clearLine);
        lcd.setCursor(0, 1);
        lcd.print("Sensor offline");

        //Handle this better
        lastReading = millis();
    }

}

void printOfficeTemp(char temp) {
    lcd.setCursor(0, 0);
    lcd.print(clearLine);
    delay(200);
    lcd.setCursor(0, 0);
    lcd.print("Office - ");
    lcd.print(temp, DEC);
}

void printRoamingTemp(char temp) {
    lcd.setCursor(0, 1);
    lcd.print(clearLine);
    delay(200);
    lcd.setCursor(0, 1);
    lcd.print("Roaming - ");
    lcd.print(temp, DEC);
}
